=========
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`__

__ https://keepachangelog.com/en/1.0.0/

0.1.5 (2019-09-27)
------------------

Fixed
~~~~~

* Raise htm.PrecisionError when encountering precision issues in edge-intersection
  solver.

0.1.4 (2019-09-27)
------------------

Fixed
~~~~~

* Workaround for precision issues in edge-intersection solver.

0.1.1 (2019-09-27)
------------------

Fixed
~~~~~

* Wrong URLs in docs

0.1.0 (2019-08-26)
------------------

Added
~~~~~

* First release.
