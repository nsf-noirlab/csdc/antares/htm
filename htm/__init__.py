# -*- coding: utf-8 -*-

"""Top-level package for HTM Utilities."""

__author__ = """Nicholas Wolf"""
__email__ = "nwolf@noao.edu"
__version__ = "0.1.5"

from . import constants
from .geometry import Vector
from .intersection import (
    get_htm_id,
    get_htm_id_level,
    get_htm_circle_region,
    PrecisionError,
)
