=====================
ANTARES HTM Utilities
=====================

ANTARES Utilities for working with Hierarchical Triangular Meshes (HTMs).

* Free software: MIT license
* Documentation: https://noao.gitlab.io/noao/antares/htm


Installation
------------

You can install the library from PyPI through `pip`:

.. code::
 
   $ pip install pyhtm

Or from source:

.. code::

   $ git clone https://gitlab.com/noao/antares/htm
   $ cd htm
   $ git checkout v0.1.3
   $ python setup.py install

Credits
-------

This package was created with Cookiecutter_ and NOAO's `cookiecutters/python-package`_ template
(forked from `audreyr/cookiecutter-pypackage`_).

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutters/python-package`: https://gitlab.com/noao/cookiecutters/python-package
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
