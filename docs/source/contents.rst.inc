User's Guide
------------

.. toctree::
   :maxdepth: 2

   installation
   tutorial

Additional Notes
----------------

.. toctree::
   :maxdepth: 2

   acknowledgements
   changelog
   license

