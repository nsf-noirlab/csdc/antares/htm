.. highlight:: shell

============
Installation
============ 

From PyPI
---------

You can install the library from PyPI through `pip`:

.. code::

   $ pip install pyhtm

From Source
-----------

The source code for ``htm`` can be obtained from its `GitLab repo`_
and installed like so:

.. parsed-literal::

    $ git clone https://gitlab.com/noao/antares/htm
    $ cd htm
    $ git checkout |version|
    $ python setup.py install


.. _GitLab repo: https://gitlab.com/noao/antares/htm

Installing for Development
--------------------------

If you plan on contributing to the development of the ANTARES HTM utilites
or if you want to run the latest (unreleased and maybe unstable) version
of the library, you may want to install in development mode:

.. parsed-literal::

    $ git clone https://gitlab.com/noao/antares/htm
    $ cd htm
    $ python setup.py develop

