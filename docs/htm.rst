htm package
===========

Subpackages
-----------

.. toctree::

   htm.geometry

Submodules
----------

htm.constants module
--------------------

.. automodule:: htm.constants
   :members:
   :undoc-members:
   :show-inheritance:

htm.intersection module
-----------------------

.. automodule:: htm.intersection
   :members:
   :undoc-members:
   :show-inheritance:

htm.node module
---------------

.. automodule:: htm.node
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: htm
   :members:
   :undoc-members:
   :show-inheritance:
