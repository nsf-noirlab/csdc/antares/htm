htm\.geometry package
=====================

Submodules
----------

htm\.geometry\.halfspace module
-------------------------------

.. automodule:: htm.geometry.halfspace
    :members:
    :undoc-members:
    :show-inheritance:

htm\.geometry\.relationships module
-----------------------------------

.. automodule:: htm.geometry.relationships
    :members:
    :undoc-members:
    :show-inheritance:

htm\.geometry\.vector module
----------------------------

.. automodule:: htm.geometry.vector
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: htm.geometry
    :members:
    :undoc-members:
    :show-inheritance:
